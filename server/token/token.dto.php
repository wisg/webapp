<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Token {
  private string $token;
  private DateTime $expiry_date;
  private int $user_id;

  public function __construct(string $token, DateTime $expiry_date, int $user_id) {
    $this->token = $token;
    $this->expiry_date = $expiry_date;
    $this->user_id = $user_id;
  }

  public function get_token() {
    return $this->token;
  }

  public function get_expiry_date() {
    return $this->expiry_date;
  }

  public function get_user_id() {
    return $this->user_id;
  }

  public function __toString() {
    return $this->token;
  }
}

 ?>
