<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "config.inc.php";

$mysqli = new mysqli(DATABASE_URL, DATABASE_USER,
            DATABASE_PASSWORD, "information_schema", DATABASE_PORT);
$result = $mysqli->query("SELECT TABLE_NAME FROM tables WHERE TABLE_NAME LIKE '".
                TABLE_PREFIX."%'");
$tables = array();
while($row = $result->fetch_assoc()) {
  $tables[] = $row["TABLE_NAME"];
}
$mysqli->close();
?>

<html>
  <head>
    <title>Daten exportieren</title>
  </head>
  <body>
    <h1>Daten exportieren</h1>
    <form action="/export/run" method="GET">
      <?php foreach($tables as $table) : ?>
        <input type="checkbox" name="tables[]" value="<?php echo $table ?>" />
        <label for="tables"><?php echo $table ?></label><br>
      <?php endforeach; ?>
      <button type="submit">Exportieren</button>
    </form>
  </body>
</html>
