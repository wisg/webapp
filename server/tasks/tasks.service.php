<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "logs/logger.php";
require_once "tasks/tasks.dal.php";
require_once "user/user.service.php";
require_once "exceptions/forbidden.exception.php";
require_once "exceptions/missing_parameters.exception.php";
require_once "exceptions/not_stored.exception.php";
require_once "bonus/bonus.service.php";
require_once "util/transactionmanager.util.php";

class TasksService {

  private Logger $logger;
  private TasksDal $dal;
  private UserService $user_service;
  private BonusService $bonus_service;

  public function __construct() {
    $this->logger = new Logger("TasksService");
    $this->dal = new TasksDal();
    $this->user_service = new UserService();
    $this->bonus_service = new BonusService();
  }

  public function create_task(int $user_id, ?int $principal, string $name, ?string $desciption, int $xp) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to create tasks.");
    if($user->get_role() == "ADMIN" && null == $prinicpal)
      throw new MissingParametersException("Admins have to provide a principal.");
    if(0 == $xp)
      throw new ParameterTypeException("XP cannot be zero.");

    if($user->get_role() != "ADMIN")
      $principal = $user->get_principal();

    return $this->dal->store_task($principal, $name, $desciption, $xp);
  }

  public function mark_done(int $user_id, int $task, array $asignees, array $boni, bool $archive_task) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to mark tasks as done.");

    $tm = TransactionManager::get_instance();
    $tm->open_transaction(false);

    try {
      foreach ($asignees as $asignee_id) {
        $asignee = $this->user_service->fetch_user($asignee_id);
        if($asignee->get_role() != "PLAYER") {
          $tm->rollback();
          throw new ParameterTypeException("Asignees have to be players.");
        }
        $id = $this->dal->add_points($task, $asignee_id);
        foreach ($boni as $bonus) {
          $this->bonus_service->assign_bonus($user_id, $id, $bonus);
        }
      }
      if($archive_task) {
        $this->dal->archive_task($task, true);
      }
    } catch (Exception $e) {
      $tm->rollback();
      throw $e;
    }

    $tm->commit();
  }

  public function fetch_tasks(int $user_id, ?int $principal) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to view the task list.");

    if($user->get_role() == "ADMIN" && null == $principal)
      throw new MissingParametersException("Admins have to provide a principal.");
    else
      $principal = $user->get_principal();

    return $this->dal->fetch_tasks($principal);
  }

  public function modify_task(int $user_id, int $id, string $name, ?string $desciption, int $xp) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to create tasks.");
    if(0 == $xp)
      throw new ParameterTypeException("XP cannot be zero.");

    $this->dal->update_task($id, $name, $desciption, $xp);
  }

}

 ?>
