<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'user/user.service.php';
include_once 'exceptions/not_authorized.exception.php';
include_once 'exceptions/missing_parameters.exception.php';
include_once 'logs/logger.php';
include_once 'util/token.util.php';

class UserController {

  private Logger $logger;
  private UserService $user_service;
  private TokenUtil $token_util;

  public function __construct() {
    $this->user_service = new UserService();
    $this->logger = new Logger("UserController");
    $this->token_util = new TokenUtil();
  }

  public function login() {
    $this->logger->debug("User login called.");

    if(!isset($_POST["username"])) throw new MissingParametersException("Username missing");
    if(!isset($_POST["password"])) throw new MissingParametersException("Password missing");

    $username = $_POST["username"];
    $password = $_POST["password"];

    try {
      $token = $this->user_service->user_login($username, $password);
      $this->logger->info($username." successfuly authenticated.");
      $json_token = array(
        "token" => $token->get_token(),
        "expiry_date" => $token->get_expiry_date()->format("d.m.Y H:i,s")
      );
      echo json_encode($json_token);
    } catch(NotAuthorizedException $not_authorized) {
      $this->logger->warn("Login for user ".$username." failed.");
      header('HTTP/1.1 401 Unauthorized');
      $GLOBALS["http_status"] = 401;
      echo "NOT AUTHORIZED";
      die();
    }
  }

  public function change_password() {
    $this->logger->debug("Change password called.");
    $user_id = $this->token_util->check_token();

    if(!isset($_POST["new_password"])) {
      throw new MissingParametersException();
    }

    $new_password = $_POST["new_password"];

    $this->user_service->change_password($user_id, $new_password);
    $this->logger->info("Password change successful");
  }

  public function create_user() {
    $this->logger->debug("Create user called.");
    $creator = $this->token_util->check_token();

    if(!isset($_POST["username"]) || !isset($_POST["role"])
      || !isset($_POST["password"])) {
      throw new MissingParametersException("Username, password or role missing");
    }

    $user = new User(0, $_POST["username"], "");
    $user->set_password($_POST["password"]);
    $user->set_role($_POST["role"]);
    if(isset($_POST["principal"])) $user->set_principal($_POST["principal"]);
    if(isset($_POST["forename"])) $user->set_forename($_POST["forename"]);
    if(isset($_POST["name"])) $user->set_name($_POST["name"]);

    try {
      $created_user = $this->user_service->create_user($creator, $user);
      $user_json = array(
        'id' => $created_user->get_id(),
        'username' => $created_user->get_username(),
        'forename' => $created_user->get_forename(),
        'name' => $created_user->get_name(),
        'principal' => $created_user->get_principal(),
        'role' => $created_user->get_role()
      );
      echo json_encode($user_json);
    } catch(NotStoredException $e) {
      $this->logger->warn("User not stored. Cause: ".$e->__toString());
      header('HTTP/1.1 500 Internal Server Error');
      $GLOBALS["http_status"] = 500;
      echo "USER NOT STORED";
    }
  }

  public function whoami() {
    $this->logger->info("WHOAMI called");

    $user_id = $this->token_util->check_token();

    $user = $this->user_service->fetch_user($user_id);
    $this->logger->debug("Fetched user".$user->get_id());

    $json_output = array(
      'id' => $user->get_id(),
      'username' => $user->get_username(),
      'role' => $user->get_role(),
      'forename' => $user->get_forename(),
      'name' => $user->get_name(),
      'principal' => $user->get_principal()
    );

    echo json_encode($json_output);
  }

  public function fetch_users() {
    $this->logger->info("fetch_users() called");

    $requester_id = $this->token_util->check_token();

    $principal = null;
    if(isset($_GET["principal"])) {
      $principal = intval($_GET["principal"]);
      if(gettype($principal) != "integer") throw new ParameterTypeException("Principal not a number.");
    }

    $users = $this->user_service->fetch_users($requester_id, $principal);

    $json_output = array();
    foreach ($users as $user) {
      $user_json = array(
        'id' => $user->get_id(),
        'username' => $user->get_username(),
        'role' => $user->get_role(),
        'forename' => $user->get_forename(),
        'name' => $user->get_name()
      );
      $json_output[] = $user_json;
    }

    echo json_encode($json_output);
  }

  public function fetch_user() {
    $requester_id = $this->token_util->check_token();
    if(!isset($_GET["id"]))
      throw new MissingParametersException("User ID to look for not given.");
    $user_id = intval($_GET["id"]);
    if(gettype($user_id) != "integer")
      throw new ParameterTypeException("User ID not given as number.");
    $user = $this->user_service->fetch_user($user_id, $requester_id);
    $output_json = array(
      'id' => $user->get_id(),
      'username' => $user->get_username(),
      'role' => $user->get_role(),
      'forename' => $user->get_forename(),
      'name' => $user->get_name()
    );

    echo json_encode($output_json);
  }

  public function change_user_data() {
    $user_id = $this->token_util->check_token();

    $username = null;
    if(isset($_POST["username"])) {
      $username = $_POST["username"];
      if(gettype($username) != "string")
        throw new ParameterTypeException("Username not given as string.");
    }

    $forename = null;
    if(isset($_POST["forename"])) {
      $forename = $_POST["forename"];
      if(gettype($forename) != "string")
        throw new ParameterTypeException("Forename not given as string.");
    }

    $name = null;
    if(isset($_POST["name"])) {
      $name = $_POST["name"];
      if(gettype($name) != "string")
        throw new ParameterTypeException("Name not given as string.");
    }

    $this->user_service->change_user_data($user_id, $username, $forename, $name);
  }

  public function change_profile_picture() {
    $user_id = $this->token_util->check_token();

    if(!isset($_POST["image"]))
      throw new MissingParametersException("No profile picture provided.");

    $picture = $_POST["image"];

    $this->user_service->change_profile_picture($user_id, $picture);
  }

  public function fetch_profile_picture() {
    $user_id = $this->token_util->check_token();

    if(!isset($_GET["owner"]))
      throw new MissingParametersException("Owner of the picture missing.");

    $owner = intval($_GET["owner"]);
    if(gettype($owner) != "integer")
      throw new ParameterTypeException("Owner not given as ID.");

    try {
      echo $this->user_service->fetch_profile_picture($user_id, $owner);
    } catch (NotFoundException $e) {
      /*
      The exception is treated special here, because otherwise a non existant
      profile picture would cause a HTTP 500 although it's a more like a 404.
      */
      header('HTTP/1.1 404 Not Found');
      $GLOBALS["http_status"] = 404;
      $logger->warn($fe->__toString());
      echo "PROFILE PICTURE NOT FOUND";
    }
  }

}

 ?>
