<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once 'util/database.util.php';
require_once 'logs/logger.php';
include_once 'config.inc.php';
include_once 'exceptions/not_found.exception.php';
include_once 'exceptions/not_stored.exception.php';
include_once 'principal/principal.dto.php';

class PrincipalDal {

  private Logger $logger;

  public function __construct() {
    $this->logger = new Logger("PrincipalDal");
  }

  public function store_principal(string $name) {
    $mysqli = create_db_connection();
    $max_id_query = "SELECT MAX(id) AS max_id FROM ".TABLE_PREFIX."principal";
    $this->logger->debug("Executing query ".$max_id_query);
    $result = $mysqli->query($max_id_query);
    if($result == false) {
      $msg = "No principals stored.";
      $this->logger->error($msg);
      throw new NotFoundException($msg);
    }
    $row = $result->fetch_assoc();
    $max_id = $row["max_id"];
    $new_id = $max_id + 1;
    $insert_query = "INSERT INTO ".TABLE_PREFIX."principal".
                    "(id,name) VALUES (".$new_id.",'".
                    $mysqli->real_escape_string($name)."')";
    $this->logger->debug("Executing query ".$insert_query);
    $result = $mysqli->query($insert_query);
    if($result == false) {
      $this->logger->error("Principal not stored.");
      throw new NotStoredException();
    }
    return new Principal($new_id,$name);
  }

  public function fetch_principals() {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."principal";
    $this->logger->debug("Executing query ".$query);
    $result = $mysqli->query($query);
    if($result == false) {
      throw new NotFoundException();
    }
    $ret_val = array();
    while($row = $result->fetch_assoc()) {
      $principal = array(
        "id" => $row["id"],
        "name" => $row["name"]
      );
      $ret_val[] = $principal;
    }
    return $ret_val;
  }

}

 ?>
