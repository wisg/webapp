/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class RankingContent {

  constructor(container) {
    this.log = new Logger("RankingContent");
    this.utils = new Utils();
    this.controller = new OverviewController();

    this.container = container;

    this.log.debug("Creating rankings within " + this.container + ".");

    $("body").append(
      "<div id=\"team-buffer\" class=\"d-none\"></div>" +
      "<div id=\"member-buffer\" class=\"d-none\"></div>"
    );

    let ref = this;
    this.controller.fetchRankings(function(rankings) {
      rankings.forEach(function(ranking) {
        ref._createRankingEntry(ranking);
      });
    }, function() {
      alert("Rankings konnten nicht gefetcht werden.");
      ref.controller.logout();
    });
  }

  _createRankingEntry(ranking) {
    this.log.debug("Creating entry for " + JSON.stringify(ranking));
    let ref = this;
    this.utils.loadTemplate("#team-buffer", "overview/team.html", function() {
      let team = $("#team-buffer .team:last-child");
      team.find(".team-name").text(ranking.team.name);
      let index = 1;
      ref.utils.loadTemplate("#member-buffer", "overview/team-member.html", function() {
        ranking.ranking.forEach(function(entry) {
          ref.log.debug("Creating entry " + JSON.stringify(entry));
          let member = $("#member-buffer .team-member:last-child").clone();
          member.find(".rank").text(index++);
          member.find(".points").text(entry.points);
          ref.controller.fetchUser(entry.user, function(user) {
            member.find(".name").text(user.username + " (" + user.forename + " " + user.name + ")");
            ref.controller.fetchProfilePicture(user.id, function(image) {
              member.find(".profile-pic img").attr("src", image);
            });
          });
          member.appendTo(team.find(".team-members"));
        }, function() {
          alert("Ranking konnte nicht geladen werden.");
          ref.controller.logout();
        });
      });
      ref.log.debug("Appending team to " + this.container);
      team.appendTo($(ref.container));
    });
  }

}
