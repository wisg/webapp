/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TeamService {

  constructor() {
    this.log = new Logger("TeamService");
    this.lsm = new LocalStorageManager();
    this.utils = new Utils();
  }

  fetchTeams(successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let ref = this;
    $.ajax({
      url: ref.lsm.getServerUrl() + "/api/0.1/teams/fetch",
      method: "GET",
      datatype: "json",
      data: {},
      headers: header,
      success: function(data, status, xhr) {
        var response = JSON.parse(data);
        ref.log.info("Teams fetched: " + data);
        successCallback(response);
      },
      error: function(xhr, status, error) {
        ref.log.warn("Teams not fetched.");
        errorCallback();
      }
    });
  }

}
