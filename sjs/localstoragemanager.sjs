/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class LocalStorageManager {

  constructor() {
    this.server_key = "server";
    this.token_key = "token";
    this.user_key = "user";
  }

  setServerUrl(url) {
    window.localStorage.setItem(this.server_key, url);
  }

  getServerUrl() {
    return window.localStorage.getItem(this.server_key);
  }

  deleteServerUrl() {
    window.localStorage.removeItem(this.server_key);
  }

  setToken(token) {
    window.localStorage.setItem(this.token_key, token);
  }

  getToken() {
    return window.localStorage.getItem(this.token_key);
  }

  deleteToken() {
    window.localStorage.removeItem(this.token_key);
  }

  setUser(user) {
    window.localStorage.setItem(this.user_key, user);
  }

   getUser() {
    return window.localStorage.getItem(this.user_key);
  }

   deleteUser() {
    window.localStorage.removeItem(this.user_key);
  }

}
